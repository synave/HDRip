# HDRip

Pr�requis
==========

Pas de pr�requis particulier. La biblioth�que Eigen utilis�e est fournie avec le code de HDRip.

Eigen
-----
 
La biblioth�que HDRip utilise la biblioth�que eigen dont vous pourrez trouver une version originale ici : https://eigen.tuxfamily.org

La version utilis�e dans HDRip est la 3.3.9. Seuls les fichiers n�cessaires ont �t� conserv�s.

Plateformes de test
------------------

Test� sous Microsoft Windows 10 et Microsoft Visual Studio Community 2019.

Compilation de la biblioth�que
===================

Sous windows
------------
Pour compiler la biblioth�que, vous pouvez ouvrir le fichier **HDRip.sln** avec Microsoft Visual Studio Community 2019. Avant la compilation, veuillez v�rifier que la solution est bien configur�e en mode Release.

Vous pouvez ensuite compiler la biblioth�que en passant par le menu "G�n�rer" -> "G�n�rer la solution".

La biblioth�que "HDRip.dll" se trouvera alors dans le r�pertoire x64\Release

TODO
====

Commentaire/Documentation