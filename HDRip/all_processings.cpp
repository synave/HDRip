// This file is part of HDRip.
// 
//     HDRip is free software: you can redistribute it and/or modify it
//     under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     HDRip is distributed in the hope that it will be useful, but
//     WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with HDRip.   If not, see <https://www.gnu.org/licenses/>.
//
// HDRip project
// Author : R�mi Synave
// Contact : remi.synave@univ-littoral.fr

#include "pch.h"

#include <iostream>
#include <chrono>
#include <cstdlib>
#include <vector>
#include <ctime>

#include <Eigen/Core>
#include <unsupported/Eigen/Splines>

#include "ImageHDR.hpp"
#include "YCurve.hpp"
#include "Conversion.hpp"
#include "all_processings.hpp"



float* exposure(float* data, unsigned int width, unsigned int height, float exposure)
{
  ImageHDR img(data, width, height);

  img.exposure(exposure);

  float* ret = new float[width * height * 3];
  memcpy(ret, img.data, width * height * 3 * sizeof(float));

  return ret;
}



float* contrast(float* data, unsigned int width, unsigned int height, float constrast)
{
  ImageHDR img(data, width, height);

  img.contrast(constrast);

  img.non_linear_to_linear();
  
  img.linear=true;
  
  float* ret = new float[width * height * 3];
  memcpy(ret, img.data, width * height * 3 * sizeof(float));
  
  return ret;
}



float* yCurve(float* data, unsigned int width, unsigned int height, float yCs, float yCb, float yCm, float yCw, float yCh)
{
  ImageHDR img(data, width, height);

  img.yCurve(yCs, yCb, yCm, yCw, yCh);
  
  img.non_linear_to_linear();
  img.linear=true;

  float* ret = new float[width * height * 3];
  memcpy(ret, img.data, width * height * 3 * sizeof(float));

  return ret;
}



float* lightnessMask(float* data, unsigned int width, unsigned int height, bool lms, bool lmb, bool lmm, bool lmw, bool lmh)
{
  ImageHDR img(data, width, height);

  img.lightnessMask(lms, lmb, lmm, lmw, lmh);

  img.non_linear_to_linear();
  img.linear=true;

  float* ret = new float[width * height * 3];
  memcpy(ret, img.data, width * height * 3 * sizeof(float));

  return ret;
}



float* saturation(float* data, unsigned int width, unsigned int height, float saturation)
{
  ImageHDR img(data, width, height);

  img.saturation(saturation);

  img.non_linear_to_linear();
  img.linear=true;

  float* ret = new float[width * height * 3];
  memcpy(ret, img.data, width * height * 3 * sizeof(float));

  return ret;
}



float* colorEditor(float* data, unsigned int width, unsigned int height, float ce_sel_light_l, float ce_sel_light_h, float ce_sel_chr_l, float ce_sel_chr_h, float ce_sel_hue_l, float ce_sel_hue_h, float ce_tol, float ce_edit_hue, float ce_edit_expo, float ce_edit_con, float ce_edit_sat, bool ce_mask)
{
  ImageHDR img(data, width, height);

  float sel_light[2] = { ce_sel_light_l, ce_sel_light_h };
  float sel_chr[2] = { ce_sel_chr_l, ce_sel_chr_h };
  float sel_hue[2] = { ce_sel_hue_l, ce_sel_hue_h };

  img.colorEditor(sel_light, sel_chr, sel_hue, ce_tol, ce_edit_hue, ce_edit_expo, ce_edit_con, ce_edit_sat, ce_mask);

  float* ret = new float[width * height * 3];
  memcpy(ret, img.data, width * height * 3 * sizeof(float));

  return ret;
}



float* full_process(float* data, unsigned int width, unsigned int height,
  float exposure,
  float contrast,
  float yCs, float yCb, float yCm, float yCw, float yCh,
  bool lms, bool lmb, bool lmm, bool lmw, bool lmh,
  float saturation,
  float ce_sel_light_l, float ce_sel_light_h, float ce_sel_chr_l, float ce_sel_chr_h, float ce_sel_hue_l, float ce_sel_hue_h, float ce_tol, float ce_edit_hue, float ce_edit_expo, float ce_edit_con, float ce_edit_sat, bool ce_mask)
{
  ImageHDR img(data, width, height);

  // ******************************************
  img.exposure(exposure);
  // ******************************************

  // ******************************************
  img.contrast(contrast);
  // ******************************************

  // ******************************************
  img.yCurve(yCs, yCb, yCm, yCw, yCh);
  // ******************************************

  // ******************************************
  img.lightnessMask(lms, lmb, lmm, lmw, lmh);
  // ******************************************
  
  // ******************************************
  img.saturation(saturation);
  // ******************************************
  
  // ******************************************
  float sel_light[2] = {ce_sel_light_l, ce_sel_light_h};
  float sel_chr[2] = {ce_sel_chr_l, ce_sel_chr_h};
  float sel_hue[2] = {ce_sel_hue_l, ce_sel_hue_h};

  img.colorEditor(sel_light, sel_chr, sel_hue, ce_tol, ce_edit_hue, ce_edit_expo, ce_edit_con, ce_edit_sat, ce_mask);
  // ******************************************


  if(img.linear==false){
    img.non_linear_to_linear();
    img.linear=true;
  }

  float *ret = new float[width*height*3];
  memcpy(ret, img.data, width * height * 3 * sizeof(float));
  
  return ret;
}

float* full_process_5CO(float* data, unsigned int width, unsigned int height,
  float exposure,
  float contrast,
  float yCs, float yCb, float yCm, float yCw, float yCh,
  bool lms, bool lmb, bool lmm, bool lmw, bool lmh,
  float saturation,
  float ce_sel_light_l_co1, float ce_sel_light_h_co1, float ce_sel_chr_l_co1, float ce_sel_chr_h_co1, float ce_sel_hue_l_co1, float ce_sel_hue_h_co1, float ce_tol_co1, float ce_edit_hue_co1, float ce_edit_expo_co1, float ce_edit_con_co1, float ce_edit_sat_co1, bool ce_mask_co1,
  float ce_sel_light_l_co2, float ce_sel_light_h_co2, float ce_sel_chr_l_co2, float ce_sel_chr_h_co2, float ce_sel_hue_l_co2, float ce_sel_hue_h_co2, float ce_tol_co2, float ce_edit_hue_co2, float ce_edit_expo_co2, float ce_edit_con_co2, float ce_edit_sat_co2, bool ce_mask_co2,
  float ce_sel_light_l_co3, float ce_sel_light_h_co3, float ce_sel_chr_l_co3, float ce_sel_chr_h_co3, float ce_sel_hue_l_co3, float ce_sel_hue_h_co3, float ce_tol_co3, float ce_edit_hue_co3, float ce_edit_expo_co3, float ce_edit_con_co3, float ce_edit_sat_co3, bool ce_mask_co3,
  float ce_sel_light_l_co4, float ce_sel_light_h_co4, float ce_sel_chr_l_co4, float ce_sel_chr_h_co4, float ce_sel_hue_l_co4, float ce_sel_hue_h_co4, float ce_tol_co4, float ce_edit_hue_co4, float ce_edit_expo_co4, float ce_edit_con_co4, float ce_edit_sat_co4, bool ce_mask_co4,
  float ce_sel_light_l_co5, float ce_sel_light_h_co5, float ce_sel_chr_l_co5, float ce_sel_chr_h_co5, float ce_sel_hue_l_co5, float ce_sel_hue_h_co5, float ce_tol_co5, float ce_edit_hue_co5, float ce_edit_expo_co5, float ce_edit_con_co5, float ce_edit_sat_co5, bool ce_mask_co5)
{
  ImageHDR img(data, width, height);

  // ******************************************
  img.exposure(exposure);
  // ******************************************

  // ******************************************
  img.contrast(contrast);
  // ******************************************

  // ******************************************
  img.yCurve(yCs, yCb, yCm, yCw, yCh);
  // ******************************************

  // ******************************************
  img.lightnessMask(lms, lmb, lmm, lmw, lmh);
  // ******************************************

  // ******************************************
  img.saturation(saturation);
  // ******************************************

  // ******************************************
  // COLOR EDITOR 0
  // TEST !!!! A SUPPRIMER NORMALEMENT.
  float sel_light[2] = { 0,100 };
  float sel_chr[2] = { 0,100 };
  float sel_hue[2] = { 0,360 };

  img.colorEditor(sel_light, sel_chr, sel_hue, 0.1, 0, 0, 0, 0, false);
  // ******************************************
  // COLOR EDITOR 1
  sel_light[0] = ce_sel_light_l_co1; sel_light[1] = ce_sel_light_h_co1;
  sel_chr[0] = ce_sel_chr_l_co1; sel_chr[1] = ce_sel_chr_h_co1;
  sel_hue[0] = ce_sel_hue_l_co1; sel_hue[1] = ce_sel_hue_h_co1;

  img.colorEditor(sel_light, sel_chr, sel_hue, ce_tol_co1, ce_edit_hue_co1, ce_edit_expo_co1, ce_edit_con_co1, ce_edit_sat_co1, ce_mask_co1);
  // ******************************************
  // COLOR EDITOR 2
  sel_light[0] = ce_sel_light_l_co2; sel_light[1] = ce_sel_light_h_co2;
  sel_chr[0] = ce_sel_chr_l_co2; sel_chr[1] = ce_sel_chr_h_co2;
  sel_hue[0] = ce_sel_hue_l_co2; sel_hue[1] = ce_sel_hue_h_co2;

  img.colorEditor(sel_light, sel_chr, sel_hue, ce_tol_co2, ce_edit_hue_co2, ce_edit_expo_co2, ce_edit_con_co2, ce_edit_sat_co2, ce_mask_co2);
  // ******************************************
  // COLOR EDITOR 3
  sel_light[0] = ce_sel_light_l_co3; sel_light[1] = ce_sel_light_h_co3;
  sel_chr[0] = ce_sel_chr_l_co3; sel_chr[1] = ce_sel_chr_h_co3;
  sel_hue[0] = ce_sel_hue_l_co3; sel_hue[1] = ce_sel_hue_h_co3;

  img.colorEditor(sel_light, sel_chr, sel_hue, ce_tol_co3, ce_edit_hue_co3, ce_edit_expo_co3, ce_edit_con_co3, ce_edit_sat_co3, ce_mask_co3);
  // ******************************************
  // COLOR EDITOR 4
  sel_light[0] = ce_sel_light_l_co4; sel_light[1] = ce_sel_light_h_co4;
  sel_chr[0] = ce_sel_chr_l_co4; sel_chr[1] = ce_sel_chr_h_co4;
  sel_hue[0] = ce_sel_hue_l_co4; sel_hue[1] = ce_sel_hue_h_co4;

  img.colorEditor(sel_light, sel_chr, sel_hue, ce_tol_co4, ce_edit_hue_co4, ce_edit_expo_co4, ce_edit_con_co4, ce_edit_sat_co4, ce_mask_co4);
  // ******************************************
  // COLOR EDITOR 5
  sel_light[0] = ce_sel_light_l_co5; sel_light[1] = ce_sel_light_h_co5;
  sel_chr[0] = ce_sel_chr_l_co5; sel_chr[1] = ce_sel_chr_h_co5;
  sel_hue[0] = ce_sel_hue_l_co5; sel_hue[1] = ce_sel_hue_h_co5;

  img.colorEditor(sel_light, sel_chr, sel_hue, ce_tol_co5, ce_edit_hue_co5, ce_edit_expo_co5, ce_edit_con_co5, ce_edit_sat_co5, ce_mask_co5);
  // ******************************************


  if (img.linear == false) {
    img.non_linear_to_linear();
    img.linear = true;
  }

  float* ret = new float[width * height * 3];
  memcpy(ret, img.data, width * height * 3 * sizeof(float));

  return ret;
}
