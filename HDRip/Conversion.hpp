// This file is part of HDRip.
// 
//     HDRip is free software: you can redistribute it and/or modify it
//     under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     HDRip is distributed in the hope that it will be useful, but
//     WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with HDRip.   If not, see <https://www.gnu.org/licenses/>.
//
// HDRip project
// Author : R�mi Synave
// Contact : remi.synave@univ-littoral.fr

#ifndef CONVERSION__HPP
#define CONVERSION__HPP

#include <cmath>
#include <iostream>
#include <tuple>



class Conversion
{
public:

  static constexpr float sRGB_to_XYZ_m[3][3] = { { 0.4124f, 0.3576f, 0.1805f},
            { 0.2126f, 0.7152f, 0.0722f},
            { 0.0193f, 0.1192f, 0.9505f} };

  // More accurate matrix
  //static constexpr float RGB_to_XYZ_m[3][3] = { { 0.412424f, 357579f, 0.180464f},
  //        { 0.212656f, 0.715158f, 0.0721856f},
  //        { 0.0193324f, 0.119193f, 0.950444f} };

  static constexpr float XYZ_to_sRGB_m[3][3] = { {3.2406f, -1.5372f, -0.4986f},
						 {-0.9689f, 1.8758f, 0.0415f},
						 {0.0557f, -0.2040f, 1.0570f} };


  static float linear_to_non_linear(float data);
  static float* linear_to_non_linear(const float* data, unsigned int length);
  static float non_linear_to_linear(float data);
  static float* non_linear_to_linear(const float* data, unsigned int length);


  static std::tuple<float, float, float> sRGB_to_XYZ(float r, float g, float b);
  // Warning data contains pixel with RGB components.
  // length is the number of pixel, not the real length of the data array.
  // The length of the data array is length*3.
  static float* sRGB_to_XYZ(const float* data, const unsigned int length);
  static float sRGB_to_Y_of_XYZ(float r, float g, float b);
  // Warning data contains pixel with RGB components.
  // length is the number of pixel, not the real length of the data array.
  // The length of the data array is length*3.
  static float* sRGB_to_Y_of_XYZ(const float* data, const unsigned int length);


  static std::tuple<float, float, float> XYZ_to_Lab(float x, float y, float z);
  // Warning data contains pixel with RGB components.
  // length is the number of pixel, not the real length of the data array.
  // The length of the data array is length*3.
  static float* XYZ_to_Lab(const float* data, const unsigned int length);

  
  static std::tuple<float, float, float> sRGB_to_Lab(float r, float g, float b);
  // Warning data contains pixel with RGB components.
  // length is the number of pixel, not the real length of the data array.
  // The length of the data array is length*3.
  static float* sRGB_to_Lab(const float* data, const unsigned int length);
  static float sRGB_to_L_of_Lab(float r, float g, float b);


  static std::tuple<float, float, float> Lab_to_LCH(float L, float a, float b);
  // Warning data contains pixel with Lab components.
  // length is the number of pixel, not the real length of the data array.
  // The length of the data array is length*3.
  static float* Lab_to_LCH(const float* data, const unsigned int length);
  static float Lab_to_C_of_LCH(float a, float b);
  static float Lab_to_H_of_LCH(float a, float b);


  static std::tuple<float, float, float> LCH_to_Lab(float L, float C, float H);
  // Warning data contains pixel with LCH components.
  // length is the number of pixel, not the real length of the data array.
  // The length of the data array is length*3.
  static float* LCH_to_Lab(const float* data, const unsigned int length);



  static std::tuple<float, float, float> Lab_to_XYZ(float L, float a, float b);
  // Warning data contains pixel with Lab components.
  // length is the number of pixel, not the real length of the data array.
  // The length of the data array is length*3.
  static float* Lab_to_XYZ(const float* data, const unsigned int length);


  
  static std::tuple<float, float, float> XYZ_to_sRGB(float x, float y, float z);
  // Warning data contains pixel with XYZ components.
  // length is the number of pixel, not the real length of the data array.
  // The length of the data array is length*3.
  static float* XYZ_to_sRGB(const float* data, const unsigned int length);

  
  // Warning data contains pixel with LCH components.
  // length is the number of pixel, not the real length of the data array.
  // The length of the data array is length*3.
  static float* LCH_to_sRGB(const float* data, const unsigned int length);
};
#endif

