// This file is part of HDRip.
// 
//     HDRip is free software: you can redistribute it and/or modify it
//     under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     HDRip is distributed in the hope that it will be useful, but
//     WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with HDRip.   If not, see <https://www.gnu.org/licenses/>.
//
// HDRip project
// Author : R�mi Synave
// Contact : remi.synave@univ-littoral.fr

#include "pch.h"

#include "YCurve.hpp"

#include <iostream>
#include <cassert>

YCurve::YCurve(const float s, const float b, const float m, const float w, const float h, const float maxChannelY)
{
  max = maxChannelY;
  Eigen::RowVectorXf knots(10);
  knots << 0.0f, 0.0f, 0.0f, 0.2f, 0.4f, 0.6f, 0.8f, 1.0f, 1.0f, 1.0f;

  Eigen::MatrixXf ctrls(7, 2);
  ctrls << 0, 0,
    10, s,
    30, b,
    50, m,
    70, w,
    90, h,
    max, 100;
  ctrls.transposeInPlace();

  spline = Eigen::Spline<float, 2, 2>(knots, ctrls);
}


Eigen::MatrixXf* YCurve::evalpts(unsigned int nb)
{
  Eigen::MatrixXf* pts = new Eigen::MatrixXf(nb, 2);

  float delta = 1.0f / (nb - 1.0f);
  unsigned int i;
  float step;

  for (i = 0, step = 0; i < nb; i++, step += delta)
  {
    //Peut mieux faire en r�cup�rant spline(step) dans une variable pour �viter de relancer le calcul
    Eigen::Spline<float, 2, 2>::PointType estim = spline(i * delta);
    (*pts)(i, 0) = estim[0];
    (*pts)(i, 1) = estim[1];
  }

  return pts;
}

