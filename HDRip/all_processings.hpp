// This file is part of HDRip.
//
//     HDRip is free software: you can redistribute it and/or modify it
//     under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     HDRip is distributed in the hope that it will be useful, but
//     WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with HDRip.   If not, see <https://www.gnu.org/licenses/>.
//
// HDRip project
// Author : R�mi Synave
// Contact : remi.synave@univ-littoral.fr

#ifndef ALL_PROCESSINGS__HPP
#define ALL_PROCESSINGS__HPP

#ifndef _LINUX

#ifdef HDRIP_EXPORTS
#define HDRIP_API __declspec(dllexport)
#else
#define HDRIP_API __declspec(dllimport)
#endif

extern "C" {
  HDRIP_API float* exposure(float* data, unsigned int width, unsigned int height, float exposure);

  HDRIP_API float* contrast(float* data, unsigned int width, unsigned int height, float contrast);

  HDRIP_API float* yCurve(float* data, unsigned int width, unsigned int height, float yCs, float yCb, float yCm, float yCw, float yCh);

  HDRIP_API float* lightnessMask(float* data, unsigned int width, unsigned int height, bool lms, bool lmb, bool lmm, bool lmw, bool lmh);

  HDRIP_API float* saturation(float* data, unsigned int width, unsigned int height, float saturation);

  HDRIP_API float* colorEditor(float* data, unsigned int width, unsigned int height, float ce_sel_light_l, float ce_sel_light_h, float ce_sel_chr_l, float ce_sel_chr_h, float ce_sel_hue_l, float ce_sel_hue_h, float ce_tol, float ce_edit_hue, float ce_edit_expo, float ce_edit_con, float ce_edit_sat, bool ce_mask);

  HDRIP_API float* full_process(float* data, unsigned int width, unsigned int height,
    float exposure,
    float contrast,
    float yCs, float yCb, float yCm, float yCw, float yCh,
    bool lms, bool lmb, bool lmm, bool lmw, bool lmh,
    float saturation,
    float ce_sel_light_l, float ce_sel_light_h, float ce_sel_chr_l, float ce_sel_chr_h, float ce_sel_hue_l, float ce_sel_hue_h, float ce_tol, float ce_edit_hue, float ce_edit_expo, float ce_edit_con, float ce_edit_sat, bool ce_mask);

  HDRIP_API float* full_process_5CO(float* data, unsigned int width, unsigned int height,
    float exposure,
    float contrast,
    float yCs, float yCb, float yCm, float yCw, float yCh,
    bool lms, bool lmb, bool lmm, bool lmw, bool lmh,
    float saturation,
    float ce_sel_light_l_co1, float ce_sel_light_h_co1, float ce_sel_chr_l_co1, float ce_sel_chr_h_co1, float ce_sel_hue_l_co1, float ce_sel_hue_h_co1, float ce_tol_co1, float ce_edit_hue_co1, float ce_edit_expo_co1, float ce_edit_con_co1, float ce_edit_sat_co1, bool ce_mask_co1,
    float ce_sel_light_l_co2, float ce_sel_light_h_co2, float ce_sel_chr_l_co2, float ce_sel_chr_h_co2, float ce_sel_hue_l_co2, float ce_sel_hue_h_co2, float ce_tol_co2, float ce_edit_hue_co2, float ce_edit_expo_co2, float ce_edit_con_co2, float ce_edit_sat_co2, bool ce_mask_co2,
    float ce_sel_light_l_co3, float ce_sel_light_h_co3, float ce_sel_chr_l_co3, float ce_sel_chr_h_co3, float ce_sel_hue_l_co3, float ce_sel_hue_h_co3, float ce_tol_co3, float ce_edit_hue_co3, float ce_edit_expo_co3, float ce_edit_con_co3, float ce_edit_sat_co3, bool ce_mask_co3,
    float ce_sel_light_l_co4, float ce_sel_light_h_co4, float ce_sel_chr_l_co4, float ce_sel_chr_h_co4, float ce_sel_hue_l_co4, float ce_sel_hue_h_co4, float ce_tol_co4, float ce_edit_hue_co4, float ce_edit_expo_co4, float ce_edit_con_co4, float ce_edit_sat_co4, bool ce_mask_co4,
    float ce_sel_light_l_co5, float ce_sel_light_h_co5, float ce_sel_chr_l_co5, float ce_sel_chr_h_co5, float ce_sel_hue_l_co5, float ce_sel_hue_h_co5, float ce_tol_co5, float ce_edit_hue_co5, float ce_edit_expo_co5, float ce_edit_con_co5, float ce_edit_sat_co5, bool ce_mask_co5);
}


#else


extern "C" {
  float* exposure(float* data, unsigned int width, unsigned int height, float exposure);

  float* contrast(float* data, unsigned int width, unsigned int height, float contrast);

  float* yCurve(float* data, unsigned int width, unsigned int height, float yCs, float yCb, float yCm, float yCw, float yCh);

  float* lightnessMask(float* data, unsigned int width, unsigned int height, bool lms, bool lmb, bool lmm, bool lmw, bool lmh);

  float* saturation(float* data, unsigned int width, unsigned int height, float saturation);

  float* colorEditor(float* data, unsigned int width, unsigned int height, float ce_sel_light_l, float ce_sel_light_h, float ce_sel_chr_l, float ce_sel_chr_h, float ce_sel_hue_l, float ce_sel_hue_h, float ce_tol, float ce_edit_hue, float ce_edit_expo, float ce_edit_con, float ce_edit_sat, bool ce_mask);

  float* full_process(float* data, unsigned int width, unsigned int height,
    float exposure,
    float contrast,
    float yCs, float yCb, float yCm, float yCw, float yCh,
    bool lms, bool lmb, bool lmm, bool lmw, bool lmh,
    float saturation,
    float ce_sel_light_l, float ce_sel_light_h, float ce_sel_chr_l, float ce_sel_chr_h, float ce_sel_hue_l, float ce_sel_hue_h, float ce_tol, float ce_edit_hue, float ce_edit_expo, float ce_edit_con, float ce_edit_sat, bool ce_mask);
}

#endif

#endif
