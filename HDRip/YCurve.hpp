// This file is part of HDRip.
// 
//     HDRip is free software: you can redistribute it and/or modify it
//     under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     HDRip is distributed in the hope that it will be useful, but
//     WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with HDRip.   If not, see <https://www.gnu.org/licenses/>.
//
// HDRip project
// Author : R�mi Synave
// Contact : remi.synave@univ-littoral.fr

#ifndef YCURVE__HPP
#define YCURVE_HPP

#include <Eigen/Core>
#include <unsupported/Eigen/Splines>
#include <vector>

class YCurve
{
public:
  YCurve(const float s, const float b, const float m, const float w, const float h, const float maxChannelY);

  Eigen::MatrixXf* evalpts(unsigned int nb);

private:
  float max;
  Eigen::Spline<float, 2, 2> spline;
};

#endif
