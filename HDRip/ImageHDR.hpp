// This file is part of HDRip.
// 
//     HDRip is free software: you can redistribute it and/or modify it
//     under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     HDRip is distributed in the hope that it will be useful, but
//     WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with HDRip.   If not, see <https://www.gnu.org/licenses/>.
//
// HDRip project
// Author : Rémi Synave
// Contact : remi.synave@univ-littoral.fr

#ifndef IMAGEHDR__HPP
#define IMAGEHDR__HPP

#include <cstring>
#include <iostream>

#include "Eigen/Core"

#include "Colorspace.hpp"


class ImageHDR {
public:
  bool linear;
  unsigned int width, height;
  float* data; //3 float per pixel. data size should be width*height*3
  float min_intensity, max_intensity;
  unsigned int colorspace;

public:
  // Constructors/Loading

  ImageHDR(float* d = NULL, unsigned int w = 0, unsigned int h = 0);

  // Destructor
  ~ImageHDR() {delete[] data;};// devrait être delete[] data;

  // save - getter/setter
  float getR(const unsigned int i, const unsigned j) const { return data[(j * width + i) * 3]; };
  float getG(const unsigned int i, const unsigned j) const { return data[(j * width + i) * 3 + 1]; };
  float getB(const unsigned int i, const unsigned j) const { return data[(j * width + i) * 3 + 2]; };

  void setR(const unsigned int i, const unsigned j, float r) { data[(j * width + i) * 3] = r; };
  void setG(const unsigned int i, const unsigned j, float g) { data[(j * width + i) * 3 + 1] = g; };
  void setB(const unsigned int i, const unsigned j, float b) { data[(j * width + i) * 3 + 2] = b; };

  void display_pixel(unsigned int i) const;
  void display_pixel(unsigned int i, unsigned int j) const;
  void display_debug() const;


  // image processing
  void linear_to_non_linear();
  void non_linear_to_linear();

  void exposure(const float ev);

  void contrast(const float c);

  void ycurve_histogram_regularization(float* colorDataY, float* colorDataFY);
  void yCurve(float s, float b, float m, float w, float h);

  void lightnessMask(bool s, bool b, bool m, bool w, bool h);

  void saturation(float s);

  void colorEditor(float* selection_lightness, float* selection_chroma, float* selection_hue, float tolerance, float edit_hue, float edit_exposure, float edit_contrast, float edit_saturation, bool mask);

private:
  // Conversions
  Eigen::VectorXf to_EigenVector() const;
};

#endif
