// This file is part of HDRip.
// 
//     HDRip is free software: you can redistribute it and/or modify it
//     under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     HDRip is distributed in the hope that it will be useful, but
//     WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with HDRip.   If not, see <https://www.gnu.org/licenses/>.
//
// HDRip project
// Author : R�mi Synave
// Contact : remi.synave@univ-littoral.fr

#ifndef MT_HISTOGRAM_REGULARIZATION__HPP
#define MT_HISTOGRAM_REGULARIZATION__HPP

class MT_histogram_regularization
{
public:
  float* data;
  unsigned int length;

  float* colorDataY;
  float* colorDataFY;

  MT_histogram_regularization(float* d = NULL, unsigned int l = 0, float* cy = NULL, float* cfy = NULL) :data(d), length(l), colorDataY(cy), colorDataFY(cfy) {};
};

#endif
