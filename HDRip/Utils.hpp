// This file is part of HDRip.
// 
//     HDRip is free software: you can redistribute it and/or modify it
//     under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     HDRip is distributed in the hope that it will be useful, but
//     WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with HDRip.   If not, see <https://www.gnu.org/licenses/>.
//
// HDRip project
// Author : R�mi Synave
// Contact : remi.synave@univ-littoral.fr

#ifndef UTILS__HPP
#define UTILS_HPP

#include <Eigen/Core>

class Utils
{
public:
  static float* interp(const float* x, const unsigned int tx, const Eigen::RowVectorXf& xd, const Eigen::RowVectorXf& yd);
  static float interp(float x, float xd1, float yd1, float xd2, float yd2);
  
  static unsigned int search(float to_search, const Eigen::RowVectorXf& data);
  static unsigned int search(float to_search, const float* data, unsigned int first, unsigned int last);

  static float* NPlinearWeightMask(float* x, unsigned int length, float xMin, float xMax, float xTolerance);
};

#endif
