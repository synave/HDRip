// This file is part of HDRip.
// 
//     HDRip is free software: you can redistribute it and/or modify it
//     under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     HDRip is distributed in the hope that it will be useful, but
//     WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with HDRip.   If not, see <https://www.gnu.org/licenses/>.
//
// HDRip project
// Author : R�mi Synave
// Contact : remi.synave@univ-littoral.fr

#ifndef MT_INTERPOLATION__HPP
#define MT_INTERPOLATION__HPP

#include <Eigen/Core>

class MT_interpolation
{
public:
  float* xInterp;
  const float* xData;
  unsigned int length;
  const float* xd;
  const float* yd;
  unsigned int length_xd_yd;

  MT_interpolation(float* xI = NULL, const float* xD = NULL, unsigned int l = 0, const float* x = NULL, const float* y = NULL, unsigned int lxdyd = 0) :xInterp(xI), xData(xD), length(l), xd(x), yd(y), length_xd_yd(lxdyd) {};
};



#endif
