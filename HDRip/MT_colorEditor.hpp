// This file is part of HDRip.
// 
//     HDRip is free software: you can redistribute it and/or modify it
//     under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     HDRip is distributed in the hope that it will be useful, but
//     WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with HDRip.   If not, see <https://www.gnu.org/licenses/>.
//
// HDRip project
// Author : R�mi Synave
// Contact : remi.synave@univ-littoral.fr

#ifndef MT_COLOREDITOR__HPP
#define MT_COLOREDITOR__HPP

#include "Colorspace.hpp"

class MT_colorEditor
{
public:
  /* This class will be used in different context for colorEditor parallelization. */
  /* Generic members. Not all of them will always be used. */
  float* data;
  unsigned int length;
  unsigned int colorspace;
  bool linear;

  float lMin, lMax;
  float cMin, cMax;
  float hMin, hMax;
  float tolerance;
  float edit_hue;
  float edit_exposure;
  float edit_contrast;
  float edit_saturation;
  bool mask;
  



  MT_colorEditor(float* d = NULL, unsigned int l = 0, unsigned int co = Colorspace::RGB, bool lin = false, float lMi = 0.0f, float lMa = 0.0f, float cMi = 0.0f, float cMa = 0.0f, float hMi = 0.0f, float hMa = 0.0f, float t = 0.0f, float eh = 0.0f, float ee = 0.0f, float ec = 0.0f, float es = 0.0f, bool m = false) :data(d), length(l), colorspace(co), linear(lin), lMin(lMi), lMax(lMa), cMin(cMi), cMax(cMa), hMin(hMi), hMax(hMa), tolerance(t), edit_hue(eh), edit_exposure(ee), edit_contrast(ec), edit_saturation(es), mask(m) {};
};

#endif