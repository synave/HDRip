CXX = g++
MT = -D_MT_=8
LINUX = -D_LINUX
INCLUDE = -I. -I./HDRip/ -I./HDRip/eigen
CXXFLAGS = -Wall -O3 $(MT) $(LINUX) -D_REENTRANT -fPIC
LDLIBS = -lm


TARGET_O = HDRip/all_processings.o HDRip/Conversion.o HDRip/ImageHDR.o HDRip/Utils.o HDRip/YCurve.o

TARGET = HDRip.so


all : $(TARGET)

$(TARGET) : $(TARGET_O)
	$(CXX) -shared -Wl,-soname,$(TARGET) -o $(TARGET) $(TARGET_O)

%.o : %.cpp
	$(CXX) $(CXXFLAGS) $(INCLUDE) -c $< -o $@

clean :
	rm -f *~ $(TARGET_O) $(TARGET)
